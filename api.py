# API test

# curl example: curl -X GET -H "Content-Type: application/json" --user zxc:cxz https://zxc.cxz.com/api/v1/users/123

import urllib2
import time

url = 'https://zxc.cxz.com/api/v1/users'
username = 'zxc'
password = 'cxz'

# Create a password manager
passman = urllib2.HTTPPasswordMgrWithDefaultRealm()

# Always use this user / pass for base url
passman.add_password(None, url, username, password)

# Create auth handler
authhandler = urllib2.HTTPBasicAuthHandler(passman)
opener = urllib2.build_opener(authhandler)

# All urlopen calls will use handler
urllib2.install_opener(opener)

# Auth is auto handled now / test polling
while True:
    pagehandle = urllib2.urlopen(url + '/123')
    print pagehandle.read()
    time.sleep(1)
